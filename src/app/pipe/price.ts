import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'price'
})
export class PricePipe implements PipeTransform {
    transform(data, values: Array<string>): any {
        const max = values[1];
        const min = values[0];
        return data.filter(item => {
            return min <= item.price && item.price < max;
        });
    }
}
