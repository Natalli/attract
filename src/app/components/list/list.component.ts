import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less']
})
export class ListComponent implements OnInit {
  @Input() selectCity: string;
  Data = [
    {id: 1, name: 'Affiliate Marketing - A Beginner\'s Guide to Earning Online', city: 1, category: 2, price: 50},
    {id: 2, name: 'Affiliate Marketing - A Beginner\'s Guide to Earning Online', city: 4, category: 1, price: 100},
    {id: 3, name: 'Affiliate Marketing - A Beginner\'s Guide to Earning Online', city: 5, category: 1, price: 1},
    {id: 4, name: 'Affiliate Marketing - A Beginner\'s Guide to Earning Online', city: 2, category: 4, price: 150},
    {id: 5, name: 'Affiliate Marketing - A Beginner\'s Guide to Earning Online', city: 3, category: 5, price: 200}
  ];
  // selectCity: string = '';
  // selectCategory: string = '';
  // selectPrice: number = 0;
  constructor() {
  }
  ngOnInit() {
  }

}
