import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.less']
})
export class CategoryComponent implements OnInit {
  @Input() selectCity: string;
  City = [
    {id: 1, name: 'City 1'},
    {id: 2, name: 'City 2'},
    {id: 3, name: 'City 3'},
    {id: 4, name: 'City 4'},
    {id: 5, name: 'City 5'},
  ];

  Category = [
    {id: 1, name: 'Category 1'},
    {id: 2, name: 'Category 2'},
    {id: 3, name: 'Category 3'},
    {id: 4, name: 'Category 4'},
    {id: 5, name: 'Category 5'},
  ];

  // selectCity: string = '';
  // selectCategory: string = '';
  // selectPrice: number = 0;0
  constructor() { }

  ngOnInit() {
  }

}
