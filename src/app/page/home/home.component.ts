import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, FormArray, FormControl} from '@angular/forms';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
    Data = [
        {id: 1, name: 'Affiliate Marketing - A Beginner\'s Guide to Earning Online', city: 1, category: 2, price: 50},
        {id: 2, name: 'Affiliate Marketing - A Beginner\'s Guide to Earning Online', city: 4, category: 1, price: 100},
        {id: 3, name: 'Affiliate Marketing - A Beginner\'s Guide to Earning Online', city: 5, category: 1, price: 1},
        {id: 4, name: 'Affiliate Marketing - A Beginner\'s Guide to Earning Online', city: 2, category: 4, price: 150},
        {id: 5, name: 'Affiliate Marketing - A Beginner\'s Guide to Earning Online', city: 3, category: 5, price: 200}
    ];
    City = [
        {id: 1, name: 'City 1'},
        {id: 2, name: 'City 2'},
        {id: 3, name: 'City 3'},
        {id: 4, name: 'City 4'},
        {id: 5, name: 'City 5'},
    ];
    Category = [
        {id: 1, name: 'Category 1'},
        {id: 2, name: 'Category 2'},
        {id: 3, name: 'Category 3'},
        {id: 4, name: 'Category 4'},
        {id: 5, name: 'Category 5'},
    ];
    min1: number;
    max1: number;
    step: number;
    stepRange: any;
    twoWayRange: any;
    selectCity: string;
    categories: Array<string>;
    selectCategory: string;
    public form: FormGroup;

    constructor(private formBuilder: FormBuilder) {
        this.selectCity = '';
        this.categories = [];
        this.min1 = 0;
        this.max1 = 250;
        this.step = 5;
        this.twoWayRange = [0, 250];
    }
    ngOnInit() {
        this.formFilter();
    }
    formFilter() {
        this.form = this.formBuilder.group({
            city: [''],
            category: new FormArray([]),
            price: ['']
        });
    }
    onCheckChange(event) {
        const formArray: FormArray = this.form.get('category') as FormArray;
        if (event.target.checked) {
            formArray.push(new FormControl(event.target.value));
        } else {
            let i = 0;
            formArray.controls.forEach((ctrl: FormControl) => {
                if (ctrl.value === event.target.value) {
                    formArray.removeAt(i);
                    return;
                }
                i++;
            });
        }
    }

    filter() {
        console.log(this.form.value);
        this.selectCity = this.form.value.city;
        this.categories = this.form.value.category;
        this.twoWayRange = this.form.value.price;
    }
}
