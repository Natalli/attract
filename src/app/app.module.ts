import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ListComponent} from './components/list/list.component';
import {CityPipe} from './pipe/city';
import {CategoryPipe} from './pipe/category';
import {PricePipe} from './pipe/price';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {CentralViewComponent} from './components/central-view/central-view.component';
import {HomeComponent} from './page/home/home.component';
import {CategoryComponent} from './components/category/category.component';
import {RangeSliderModule} from 'ngx-rangeslider-component';

@NgModule({
    declarations: [
        AppComponent,
        ListComponent,
        CityPipe,
        CategoryPipe,
        PricePipe,
        HeaderComponent,
        FooterComponent,
        CentralViewComponent,
        HomeComponent,
        CategoryComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        RangeSliderModule,
        ReactiveFormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
